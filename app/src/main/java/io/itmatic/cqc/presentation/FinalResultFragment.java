package io.itmatic.cqc.presentation;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import io.itmatic.cqc.MainActivity;
import io.itmatic.cqc.R;
import io.itmatic.cqc.model.QuizPage;
import io.itmatic.cqc.model.QuizQuestion;
import io.itmatic.cqc.utills.Helper;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FinalResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FinalResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FinalResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LinearLayout linearLayoutQuizQuestion;
    private OnFragmentInteractionListener mListener;
    List<EditText> allEditText = new ArrayList<EditText>();
    private List<QuizPage> allQuestionPage;
    private int i = 0;
    private ArrayList<Integer> allAnswerId = new ArrayList<>();
    private ArrayList<String> allAnswer = new ArrayList<>();
    private  RequestBody formBody;
    private  FormBody.Builder  formBuilder=new FormBody.Builder();

    public FinalResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FinalResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FinalResultFragment newInstance(String param1, String param2) {
        FinalResultFragment fragment = new FinalResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_final_result, container, false);
        linearLayoutQuizQuestion = (LinearLayout) view.findViewById(R.id.fl_final_result);
        allQuestionPage = ((MainActivity) getActivity()).questionpage;
        for (int i = 0; i < allQuestionPage.size(); i++) {
            QuizPage quizQuestion = allQuestionPage.get(i);
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("final");
            ArrayList<QuizQuestion> list = quizQuestion.getQuestions();
            for (int j = 0; j < list.size(); j++) {
                QuizQuestion singleQuizQuestion = list.get(j);
                createTextView(singleQuizQuestion.getQ());
                createTextView(singleQuizQuestion.getAnswer());

                formBody = formBuilder
                 .add(String.valueOf(singleQuizQuestion.getId()), singleQuizQuestion.getAnswer())
                        .build();
                //   checkQuestionType(singleQuizQuestion.getType(), singleQuizQuestion.getOptions());

            }
        }

        createFinishButton("Finish");

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    void createTextView(String question) {

        TextView questionTextView = new TextView(getActivity());
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params1.setMargins(0, 10, 0, 0);
        questionTextView.setLayoutParams(params1);
        questionTextView.setText(question);
        questionTextView.setTextAppearance(getActivity(), R.style.textview);
        linearLayoutQuizQuestion.addView(questionTextView);
    }

    private void createRadioButton(String[] options) {

        final RadioButton[] rb = new RadioButton[options.length];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setPadding(0, 13, 13, 13);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        for (int i = 0; i < options.length; i++) {
            rb[i] = new RadioButton(getActivity());
            rb[i].setText(" " + options[i]
                    + "    " + "");
            rb[i].setId(i + 100);
            rg.addView(rb[i]);
        }
        linearLayoutQuizQuestion.addView(rg);//you add the whole RadioGroup to the layout

    }

    void checkQuestionType(String type, String[] options) {

        switch (type) {
            case "radio": {
                createRadioButton(options);
                break;
            }

            case "text": {
                createEditText(false, i);
                i++;
                break;
            }

            case "number": {
                createEditText(true, i);
                i++;
                break;
            }

            case "file": {
                createUploadButton(options);
                break;
            }
        }

    }

    void createFinishButton(String text) {
        LinearLayout layout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params1);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        AppCompatButton btnNext = new AppCompatButton(getActivity());
        btnNext.setLayoutParams(params);
        btnNext.setText(text);
        layout.setGravity(Gravity.BOTTOM);
        btnNext.setTextAppearance(getActivity(), R.style.button);
        btnNext.setBackgroundResource(R.drawable.button_design);
        layout.addView(btnNext);
        linearLayoutQuizQuestion.addView(layout);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             submitFinalResult(formBody);
            }
        });
    }

    void createUploadButton(String[] options) {
        Button uploadFile = new Button(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 20, 0, 0);
        params.height = 80;
        uploadFile.setLayoutParams(params);
        uploadFile.setPadding(10, 0, 10, 0);
        uploadFile.setText("Upload File");
        uploadFile.setTextAppearance(getActivity(), R.style.button);
        uploadFile.setBackgroundResource(R.drawable.button_design);
        linearLayoutQuizQuestion.addView(uploadFile);

        uploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    public void createEditText(boolean isSetType, int editTextNumber) {

/*
        String[] strings = new String[((MainActivity) getActivity()).allEditText.size()];

        if (editTextNumber < ((MainActivity) getActivity()).allEditText.size())
            strings[editTextNumber] = ((MainActivity) getActivity()).allEditText.get(editTextNumber).getText().toString();*/
        EditText enterText = new EditText(getActivity());
        //     enterText.setText(strings[editTextNumber]);
        enterText.setTextAppearance(getActivity(), R.style.edittext);
        if (isSetType) {
            enterText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        linearLayoutQuizQuestion.addView(enterText);


    }

    protected void saveFinalResult(int id, String token) {


        Call<String> call = Helper.getFilmyApiService().savefinalanswer(allAnswerId, allAnswer);

        call.enqueue(new Callback<String>() {


            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());


                        String msg = jObjError.getString("message");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

                }

            }


            @Override
            public void onFailure(Call<String> call, Throwable t) {
                if (t instanceof IOException)

                {


                }
            }


        });
    }


  public  void submitFinalResult( RequestBody formBody )
    {
        OkHttpClient client = new OkHttpClient();


        final Request request = new Request.Builder()
                .url("http://cqc.itmatic.io/submit.php")
                .post(formBody)
                .build();

              client.newCall(request).enqueue(new okhttp3.Callback() {
                    @Override
                    public void onFailure(okhttp3.Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {

                        if(response!=null)
                        {
                            String value=response.body().string();
                            String url;
                            try {
                                JSONObject Jobject = new JSONObject(value);
                                 url=Jobject.getString("doc_url");
                                ((MainActivity)getActivity()).loadFinalOutput(url);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }


                });





    }


}
