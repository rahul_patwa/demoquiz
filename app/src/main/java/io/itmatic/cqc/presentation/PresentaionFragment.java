package io.itmatic.cqc.presentation;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.itmatic.cqc.MainActivity;
import io.itmatic.cqc.R;
import io.itmatic.cqc.model.QuizPage;
import io.itmatic.cqc.model.QuizQuestion;
import io.itmatic.cqc.utills.RealPathUtil;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PresentaionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PresentaionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PresentaionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "quiz";
    private static final String ARG_PARAM2 = "page";
    private static final String ARG_PARAM3 = "submit";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;

    private OnFragmentInteractionListener mListener;
    private LinearLayout linearLayoutQuizQuestion;
    private int quizNumber;
    private boolean isSubmit;
    private String nextButtonTag = "Next Question";
    private String submitButtonTag = "Submit";

    private List<QuizPage> questionpage;
    private List<QuizPage> allQuestionPage;
    public static String realPath;
    private File file;
    private Uri selectedImageUri;
    private QuizQuestion singleQuizQuestion;
    private ArrayList<QuizQuestion> singlepagelist;

    public PresentaionFragment() {
        // Required empty public constructor
    }


    public static PresentaionFragment newInstance(QuizPage quizQuestion, int value, boolean submit) {
        PresentaionFragment fragment = new PresentaionFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, quizQuestion);

        args.putInt(ARG_PARAM2, value);
        args.putBoolean(ARG_PARAM3, submit);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_presentaion, container, false);
        linearLayoutQuizQuestion = (LinearLayout) view.findViewById(R.id.ll_quiz_question_container);
        QuizPage quizQuestion = (QuizPage) getArguments().get(ARG_PARAM1);
        quizNumber = (int) getArguments().get(ARG_PARAM2);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(quizQuestion.getTitle());
        singlepagelist = quizQuestion.getQuestions();
        for (int i = 0; i < singlepagelist.size(); i++) {
            singleQuizQuestion = singlepagelist.get(i);
            createTextView(singleQuizQuestion.getQ(), singleQuizQuestion.getId());
            checkQuestionType(singleQuizQuestion.getType(), singleQuizQuestion.getOptions(), singleQuizQuestion.getId(),singleQuizQuestion.getAnswer());

        }
        isSubmit = (boolean) getArguments().get(ARG_PARAM3);
        if (isSubmit) {
            createNextButton(submitButtonTag);
        } else {
            createNextButton(nextButtonTag);
        }


        return view;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // SDK < API11
        if (requestCode == 10 && resultCode == -1)

        {

            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getPath(getActivity(), data.getData());


            file = new File(realPath);
            selectedImageUri = Uri.fromFile(file);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImageUri);
                //  profile.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (requestCode == 3 && resultCode == -1) {

            Bitmap image = (Bitmap) data.getExtras().get("data");

            File f = new File(getActivity().getCacheDir(), "image");
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                fos.write(bitmapdata);

                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            file = f;

            //   profile.setImageBitmap(image);


        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    void createTextView(String question, int id) {

        TextView questionTextView = new TextView(getActivity());
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params1.setMargins(0, 10, 0, 0);
        questionTextView.setLayoutParams(params1);
        questionTextView.setText(question);
        questionTextView.setTextAppearance(getActivity(), R.style.textview);


        linearLayoutQuizQuestion.addView(questionTextView);
    }

    private void createRadioButton(String[] options, int id) {

        final RadioButton[] rb = new RadioButton[options.length];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setId(id);
        rg.setPadding(0, 13, 13, 13);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        for (int i = 0; i < options.length; i++) {
            rb[i] = new RadioButton(getActivity());
            rb[i].setText(" " + options[i]
                    + "    " + "");
            rb[i].setId(i + 100);
            rg.addView(rb[i]);
        }
        linearLayoutQuizQuestion.addView(rg);//you add the whole RadioGroup to the layout

    }

    void checkQuestionType(String type, String[] options, int id,String answer) {

        switch (type) {
            case "radio": {
                createRadioButton(options, id);
                break;
            }

            case "text": {
                createEditText(false, id,answer);
                break;
            }

            case "number": {
                createEditText(true, id,answer);
                break;
            }

            case "file": {
                createUploadButton(options, id);
                break;
            }
        }

    }

    void createNextButton(String text) {
        LinearLayout layout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.setLayoutParams(params1);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        AppCompatButton btnNext = new AppCompatButton(getActivity());
        btnNext.setLayoutParams(params);
        btnNext.setText(text);
        layout.setGravity(Gravity.BOTTOM);
        btnNext.setTextAppearance(getActivity(), R.style.button);
        btnNext.setBackgroundResource(R.drawable.button_design);
        layout.addView(btnNext);
        linearLayoutQuizQuestion.addView(layout);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSubmit) {

                    prepareFinalAnswer();
                    ((MainActivity) getActivity()).loadFragment(quizNumber + 1, true);
                } else {
                    prepareFinalAnswer();
                    ((MainActivity) getActivity()).loadFinalFragment();
                }

            }
        });
    }

    void createUploadButton(String[] options, int id) {
        Button uploadFile = new Button(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 30, 0, 0);
        params.height = 80;
        uploadFile.setLayoutParams(params);
        uploadFile.setPadding(10, 0, 10, 0);
        uploadFile.setText("Upload File");
        uploadFile.setTextAppearance(getActivity(), R.style.button);
        uploadFile.setBackgroundResource(R.drawable.button_design);
        linearLayoutQuizQuestion.addView(uploadFile);

        uploadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }


    public void createEditText(boolean isSetType, int id,String answer) {

        EditText enterText = new EditText(getActivity());
        enterText.setId(id);
        enterText.setTextAppearance(getActivity(), R.style.edittext);
        ((MainActivity) getActivity()).allEditText.add(enterText);
        if (isSetType) {
            enterText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        if(answer!=null)
        {
            enterText.setText(answer);
        }
        linearLayoutQuizQuestion.addView(enterText);

    }


    public void prepareFinalAnswer() {
        final int childCount = linearLayoutQuizQuestion.getChildCount();
        for (int i = 1; i <= childCount; i++) {
            View v = linearLayoutQuizQuestion.getChildAt(i);
            if (v instanceof EditText) {
                for (int j = 0; j < singlepagelist.size(); j++) {
                    QuizQuestion singleQuestion = singlepagelist.get(j);

                    if (v.getId() == singleQuestion.getId()) {
                        singleQuestion.setAnswer(((EditText) v).getText().toString());
                    }

                }
            } else if (v instanceof RadioGroup) {
                RadioGroup radioGroup = (RadioGroup) v;
                for (int j = 0; j < singlepagelist.size(); j++) {
                    QuizQuestion singleQuestion = singlepagelist.get(j);

                    if (radioGroup.getId() == singleQuestion.getId()) {
                        int RadioButtonId = radioGroup.getCheckedRadioButtonId();
                        if(RadioButtonId!=-1)
                        {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(RadioButtonId);
                            singleQuestion.setAnswer(radioButton.getText().toString());
                        }

                    }

                }

            }

        }
    }


}
