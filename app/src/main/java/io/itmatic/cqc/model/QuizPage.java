package io.itmatic.cqc.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 06-10-2017.
 */

public class QuizPage implements Serializable {

    private String title;
    private  int id;
    private ArrayList<QuizQuestion> questions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<QuizQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuizQuestion> questions) {
        this.questions = questions;
    }
}
