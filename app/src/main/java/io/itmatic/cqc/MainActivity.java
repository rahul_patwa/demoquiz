package io.itmatic.cqc;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.FrameLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import io.itmatic.cqc.model.QuizPage;
import io.itmatic.cqc.presentation.FinalOutputFragment;
import io.itmatic.cqc.presentation.FinalResultFragment;
import io.itmatic.cqc.presentation.PresentaionFragment;
import io.itmatic.cqc.utills.Helper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private FrameLayout linearLayoutQuizQuestion;
    public static List<QuizPage> questionpage;
    public ArrayList<EditText> singlePageEditText=new ArrayList<EditText>();
    public ArrayList<EditText> allEditText = new ArrayList<EditText>();
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;


    private File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayoutQuizQuestion = (FrameLayout) findViewById(R.id.ll_main_quiz_question_container);
        quizDetail();

    }

    public void quizDetail() {

        Call<List<QuizPage>> call = Helper.getFilmyApiService().getquestionary();

        call.enqueue(new Callback<List<QuizPage>>() {

            @Override
            public void onResponse(Call<List<QuizPage>> call, Response<List<QuizPage>> response) {


                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
              /*      //DO ERROR HANDLING HERE
                    return;*/
                } else {

                    questionpage = response.body();
                    loadFragment(0, false);


                }
            }

            @Override
            public void onFailure(Call<List<QuizPage>> call, Throwable t) {


                if (t instanceof IOException)

                {


                }
            }


        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == 64206) && resultCode == -1) {



        }

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }


    public boolean loadFragment(int value, boolean isAddToBackStack) {
        boolean isSubmit = false;
        if (value < questionpage.size()) {
            if (value == questionpage.size() - 1) {
                isSubmit = true;
            }
            QuizPage question = questionpage.get(value);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(linearLayoutQuizQuestion.getId(), PresentaionFragment.newInstance(question, value, isSubmit), "someTag1");
            if (isAddToBackStack) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction.commit();
            return true;
        }
        return false;
    }


    public void loadFinalFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(linearLayoutQuizQuestion.getId(), FinalResultFragment.newInstance("a", "b"), "someTag1");

        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }


    public void loadFinalOutput(String url ) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(linearLayoutQuizQuestion.getId(), FinalOutputFragment.newInstance(url, "b"), "someTag1");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library","Remove Photo", "Cancel"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 3);
                } else if (items[item].equals("Choose from Library")) {
                    openGallery(10);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();

                }else if(items[item].equals("Remove Photo"))
                {

                    file=null;
                }
            }
        });
        builder.show();
    }

    public void openGallery(int req_code) {

        Intent intent = new Intent();

        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select file to upload "), req_code);

    }

}
