package io.itmatic.cqc.utills;


import io.itmatic.cqc.api.FilmyApiInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hardeep on 14/03/17.
 */

public class Helper {

    public static FilmyApiInterface getFilmyApiService() {
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl("http://cqc.itmatic.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FilmyApiInterface filmyApiService = retrofit.create(FilmyApiInterface.class);
        return filmyApiService;
    }

}
