package io.itmatic.cqc.api;

import java.util.ArrayList;
import java.util.List;

import io.itmatic.cqc.model.QuizPage;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by hardeep on 14/02/17.
 */
public interface FilmyApiInterface {

    @GET("demo_quiz.json")
    Call<List<QuizPage>> getquestionary();

    @FormUrlEncoded
    @POST("submit.php")
    Call<String> savefinalanswer(@Field("id") ArrayList<Integer> id , @Path("value")  ArrayList<String> answer);
/*
    @GET("user/near/by.json")
    Call<List<NearByFriends>> getNearByFriend(@Query("token") String token, @Query("latitude") String latitude, @Query("longitude") String longitude, @Query("filter") int filter);

    @FormUrlEncoded
    @POST("user/{id}/follow.json")
    Call<String> followrequest(@Field("token") String token,
                               @Path("id") int id);
    @FormUrlEncoded
    @POST("user/{id}/unfollow.json")
    Call<String> unfollowrequest(@Field("token") String token, @Path("id") int id);

    @GET("user/followers.json")
    Call<List<Follower>> getfollower(@Query("token") String token, @Query("offset") String offset, @Query("page_number") String page);


    @GET("user/following.json")
    Call<List<User>> getfollowing(@Query("token") String token, @Query("offset") String offset, @Query("page_number") String page);

    @GET("city/.json")
    Call<List<City>> getcity(@Query("token") String token);


    @FormUrlEncoded
    @POST("location/update.json")
    Call<String> updatelocation(@Field("token") String token,
                                @Field("latitude") String latitude, @Field("longitude") String longitute, @Field("name") String name);


    @GET("gallery/all.json")
    Call<List<Gallery>> getgalaryall(@Query("token") String token);

    @GET("gallery/.json")
    Call<List<Gallery>> getgalary(@Query("token") String token);

    @FormUrlEncoded
    @POST("gallery/update.json")
    Call<User> updategallery(@Field("token") String token,
                             @Field("selected_images") String selected_images);


    @GET("profile.json")
    Call<User> getprofile(@Query("token") String token);*/

}
